package com.iwant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ELearningManagementApplication {

	private static final Logger log = LoggerFactory.getLogger(ELearningManagementApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ELearningManagementApplication.class, args);

		log.info("......e-learning-management service started......");
	}

}
