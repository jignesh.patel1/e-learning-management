package com.iwant.Repo;


import com.iwant.Models.contentdetail.ContentModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ContentRepository extends MongoRepository<ContentModel, ObjectId>{

}
