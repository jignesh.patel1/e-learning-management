package com.iwant.Repo;

import com.iwant.Models.InstituteModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;


public interface InstituteRepository extends MongoRepository<InstituteModel, ObjectId>{

}
