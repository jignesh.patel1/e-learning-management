package com.iwant.Repo;


import com.iwant.Models.SvodProductModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SvodProductRepository extends MongoRepository<SvodProductModel, ObjectId>{

}
