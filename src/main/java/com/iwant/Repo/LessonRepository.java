package com.iwant.Repo;

import com.iwant.Models.LessonModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;


public interface LessonRepository extends MongoRepository<LessonModel, ObjectId> {

}
