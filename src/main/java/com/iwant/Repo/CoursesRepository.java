package com.iwant.Repo;


import com.iwant.Models.CoursesModel;
import com.iwant.Models.InstituteModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CoursesRepository extends MongoRepository<CoursesModel, ObjectId> {


}
