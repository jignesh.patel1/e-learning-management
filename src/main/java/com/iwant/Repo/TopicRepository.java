package com.iwant.Repo;


import com.iwant.Models.TopicsModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TopicRepository extends MongoRepository<TopicsModel, ObjectId>{

}
