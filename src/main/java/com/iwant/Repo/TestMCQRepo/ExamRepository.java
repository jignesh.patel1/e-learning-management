package com.iwant.Repo.TestMCQRepo;

import com.iwant.Models.TestMCQInfo.ExamAttempts;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ExamRepository extends MongoRepository<ExamAttempts, ObjectId> {
}
