package com.iwant.Repo.TestMCQRepo;

import com.iwant.Models.TestMCQInfo.CertificationExams;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface CertificationRepository extends MongoRepository<CertificationExams, ObjectId> {
}
