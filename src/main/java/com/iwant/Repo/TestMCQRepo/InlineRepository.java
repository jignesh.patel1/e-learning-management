package com.iwant.Repo.TestMCQRepo;

import com.iwant.Models.TestMCQInfo.InlineExams;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface InlineRepository extends MongoRepository<InlineExams, ObjectId> {
}
