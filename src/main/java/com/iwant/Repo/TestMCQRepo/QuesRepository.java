package com.iwant.Repo.TestMCQRepo;


import com.iwant.Models.TestMCQInfo.QuesLibrary;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QuesRepository extends MongoRepository<QuesLibrary, ObjectId>{
}
