package com.iwant.Repo.TestMCQRepo;

import com.iwant.Models.TestMCQInfo.PracticeExams;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface PracticeRepository extends MongoRepository<PracticeExams, ObjectId> {
}
