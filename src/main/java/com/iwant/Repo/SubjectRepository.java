package com.iwant.Repo;

import com.iwant.Models.SubjectModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;


public interface SubjectRepository extends MongoRepository<SubjectModel, ObjectId> {

}
