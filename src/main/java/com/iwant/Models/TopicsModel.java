package com.iwant.Models;


import com.iwant.Models.Catalogue.BannerModel;
import com.iwant.Models.Catalogue.ThumbnailModel;
import com.iwant.Models.contentdetail.ContentModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "topics")
public class TopicsModel {

    @MongoId
    private ObjectId topicId;

    @Indexed
    private String title;

    private String intro;

    private String description;

    private String contentType;

    private ArrayList<StreamInfo> streamInfo;

    private ArrayList<BannerModel> bannerImage;

    private ArrayList<ThumbnailModel> thumbImage;

    private ArrayList<MovieInfo> movieInfo;

    private String tutorialInfo;

    private List<ContentModel> contentRef;

    private boolean isFree;

    private List<String> availability;

    private boolean canPreview;

    private Integer previewDuration;

    private String tvodPrice;

    private Date publishStart;

    private Date publishEnd;



    public TopicsModel() {
    }

    public ObjectId getTopicId() {
        return topicId;
    }

    public void setTopicId(ObjectId topicId) {
        this.topicId = topicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public ArrayList<StreamInfo> getStreamInfo() {
        return streamInfo;
    }

    public void setStreamInfo(ArrayList<StreamInfo> streamInfo) {
        this.streamInfo = streamInfo;
    }

    public ArrayList<BannerModel> getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(ArrayList<BannerModel> bannerImage) {
        this.bannerImage = bannerImage;
    }

    public ArrayList<ThumbnailModel> getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(ArrayList<ThumbnailModel> thumbImage) {
        this.thumbImage = thumbImage;
    }

    public ArrayList<MovieInfo> getMovieInfo() {
        return movieInfo;
    }

    public void setMovieInfo(ArrayList<MovieInfo> movieInfo) {
        this.movieInfo = movieInfo;
    }

    public String getTutorialInfo() {
        return tutorialInfo;
    }

    public void setTutorialInfo(String tutorialInfo) {
        this.tutorialInfo = tutorialInfo;
    }

    public List<ContentModel> getContentRef() {
        return contentRef;
    }

    public void setContentRef(List<ContentModel> contentRef) {
        this.contentRef = contentRef;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public List<String> getAvailability() {
        return availability;
    }

    public void setAvailability(List<String> availability) {
        this.availability = availability;
    }

    public boolean isCanPreview() {
        return canPreview;
    }

    public void setCanPreview(boolean canPreview) {
        this.canPreview = canPreview;
    }

    public Integer getPreviewDuration() {
        return previewDuration;
    }

    public void setPreviewDuration(Integer previewDuration) {
        this.previewDuration = previewDuration;
    }

    public String getTvodPrice() {
        return tvodPrice;
    }

    public void setTvodPrice(String tvodPrice) {
        this.tvodPrice = tvodPrice;
    }

    public Date getPublishStart() {
        return publishStart;
    }

    public void setPublishStart(Date publishStart) {
        this.publishStart = publishStart;
    }

    public Date getPublishEnd() {
        return publishEnd;
    }

    public void setPublishEnd(Date publishEnd) {
        this.publishEnd = publishEnd;
    }

    @Override
    public String toString() {
        return "TopicsModel{" +
                "topicId=" + topicId +
                ", title='" + title + '\'' +
                ", intro='" + intro + '\'' +
                ", description='" + description + '\'' +
                ", contentType='" + contentType + '\'' +
                ", streamInfo=" + streamInfo +
                ", bannerImage=" + bannerImage +
                ", thumbImage=" + thumbImage +
                ", movieInfo=" + movieInfo +
                ", tutorialInfo='" + tutorialInfo + '\'' +
                ", contentRef=" + contentRef +
                ", isFree=" + isFree +
                ", availability=" + availability +
                ", canPreview=" + canPreview +
                ", previewDuration=" + previewDuration +
                ", tvodPrice='" + tvodPrice + '\'' +
                ", publishStart=" + publishStart +
                ", publishEnd=" + publishEnd +
                '}';
    }
}
