package com.iwant.Models;


public class Gradient {

    private String startColor;
    private String startColorOpacity;
    private String middleColor;
    private String middleColorOpacity;
    private String endColor;
    private String endColorOpacity;

    public Gradient() {
    }

    public String getStartColor() {
        return startColor;
    }

    public void setStartColor(String startColor) {
        this.startColor = startColor;
    }

    public String getStartColorOpacity() {
        return startColorOpacity;
    }

    public void setStartColorOpacity(String startColorOpacity) {
        this.startColorOpacity = startColorOpacity;
    }

    public String getMiddleColor() {
        return middleColor;
    }

    public void setMiddleColor(String middleColor) {
        this.middleColor = middleColor;
    }

    public String getMiddleColorOpacity() {
        return middleColorOpacity;
    }

    public void setMiddleColorOpacity(String middleColorOpacity) {
        this.middleColorOpacity = middleColorOpacity;
    }

    public String getEndColor() {
        return endColor;
    }

    public void setEndColor(String endColor) {
        this.endColor = endColor;
    }

    public String getEndColorOpacity() {
        return endColorOpacity;
    }

    public void setEndColorOpacity(String endColorOpacity) {
        this.endColorOpacity = endColorOpacity;
    }

    @Override
    public String toString() {
        return "Gradient{" +
                "startColor='" + startColor + '\'' +
                ", startColorOpacity='" + startColorOpacity + '\'' +
                ", middleColor='" + middleColor + '\'' +
                ", middleColorOpacity='" + middleColorOpacity + '\'' +
                ", endColor='" + endColor + '\'' +
                ", endColorOpacity='" + endColorOpacity + '\'' +
                '}';
    }
}
