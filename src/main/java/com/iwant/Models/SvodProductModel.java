package com.iwant.Models;

import com.iwant.Models.Catalogue.ThumbnailModel;
import com.iwant.Models.contentdetail.ContentModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

/**
 * Created by jignesh on 8/11/19.
 */

@Document(collection = "svodproduct")
public class SvodProductModel {

    @MongoId
    private ObjectId svodProductId;

    @Indexed
    private String intro;

    private String description;

    private List<ThumbnailModel> thumbImage;

    private String contentType;

    private List<ContentModel> contentList;

    private boolean trialAllowed;

    private String trialPlaybackDuration;

    private String subscriptionTypes; // Month, Quarter, Year, Term

    private String monthlyPrice;
    private String quarterlyPrice;
    private String yearlyPrice;
    private String termPrice;

    private int concurrentSessions;
    private int termFinishOn; // Terms Ending Month Number

    private String extraInfo1;
    private String extraInfo2;

    public SvodProductModel() {
    }

    public ObjectId getSvodProductId() {
        return svodProductId;
    }

    public void setSvodProductId(ObjectId svodProductId) {
        this.svodProductId = svodProductId;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ThumbnailModel> getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(List<ThumbnailModel> thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public List<ContentModel> getContentList() {
        return contentList;
    }

    public void setContentList(List<ContentModel> contentList) {
        this.contentList = contentList;
    }

    public boolean isTrialAllowed() {
        return trialAllowed;
    }

    public void setTrialAllowed(boolean trialAllowed) {
        this.trialAllowed = trialAllowed;
    }

    public String getTrialPlaybackDuration() {
        return trialPlaybackDuration;
    }

    public void setTrialPlaybackDuration(String trialPlaybackDuration) {
        this.trialPlaybackDuration = trialPlaybackDuration;
    }

    public String getSubscriptionTypes() {
        return subscriptionTypes;
    }

    public void setSubscriptionTypes(String subscriptionTypes) {
        this.subscriptionTypes = subscriptionTypes;
    }

    public String getMonthlyPrice() {
        return monthlyPrice;
    }

    public void setMonthlyPrice(String monthlyPrice) {
        this.monthlyPrice = monthlyPrice;
    }

    public String getQuarterlyPrice() {
        return quarterlyPrice;
    }

    public void setQuarterlyPrice(String quarterlyPrice) {
        this.quarterlyPrice = quarterlyPrice;
    }

    public String getYearlyPrice() {
        return yearlyPrice;
    }

    public void setYearlyPrice(String yearlyPrice) {
        this.yearlyPrice = yearlyPrice;
    }

    public String getTermPrice() {
        return termPrice;
    }

    public void setTermPrice(String termPrice) {
        this.termPrice = termPrice;
    }

    public int getConcurrentSessions() {
        return concurrentSessions;
    }

    public void setConcurrentSessions(int concurrentSessions) {
        this.concurrentSessions = concurrentSessions;
    }

    public int getTermFinishOn() {
        return termFinishOn;
    }

    public void setTermFinishOn(int termFinishOn) {
        this.termFinishOn = termFinishOn;
    }

    public String getExtraInfo1() {
        return extraInfo1;
    }

    public void setExtraInfo1(String extraInfo1) {
        this.extraInfo1 = extraInfo1;
    }

    public String getExtraInfo2() {
        return extraInfo2;
    }

    public void setExtraInfo2(String extraInfo2) {
        this.extraInfo2 = extraInfo2;
    }


    @Override
    public String toString() {
        return "SvodProductModel{" +
                "svodProductId=" + svodProductId +
                ", intro='" + intro + '\'' +
                ", description='" + description + '\'' +
                ", thumbImage=" + thumbImage +
                ", contentType='" + contentType + '\'' +
                ", contentList=" + contentList +
                ", trialAllowed=" + trialAllowed +
                ", trialPlaybackDuration='" + trialPlaybackDuration + '\'' +
                ", subscriptionTypes='" + subscriptionTypes + '\'' +
                ", monthlyPrice='" + monthlyPrice + '\'' +
                ", quarterlyPrice='" + quarterlyPrice + '\'' +
                ", yearlyPrice='" + yearlyPrice + '\'' +
                ", termPrice='" + termPrice + '\'' +
                ", concurrentSessions=" + concurrentSessions +
                ", termFinishOn=" + termFinishOn +
                ", extraInfo1='" + extraInfo1 + '\'' +
                ", extraInfo2='" + extraInfo2 + '\'' +
                '}';
    }
}
