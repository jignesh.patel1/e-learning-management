package com.iwant.Models;


import com.iwant.Models.Catalogue.BannerModel;
import com.iwant.Models.Catalogue.ThumbnailModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "Courses")
public class CoursesModel implements Serializable{


    @MongoId
    private ObjectId courseId;

    @Indexed
    private String title;

/*only one institute so instituteModel is taken */
    private InstituteModel institute;

/*can have number of subjects thats why list<subjectmodel> taken */
    private List<SubjectModel> subject;

    private String intro;

    private String description;

    private ArrayList<BannerModel> bannerImage;

    private ArrayList<ThumbnailModel> thumbnailImage;

    public CoursesModel() {
    }

    public ObjectId getCourseId() {
        return courseId;
    }

    public void setCourseId(ObjectId courseId) {
        this.courseId = courseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public InstituteModel getInstitute() {
        return institute;
    }

    public void setInstitute(InstituteModel institute) {
        this.institute = institute;
    }

    public List<SubjectModel> getSubject() {
        return subject;
    }

    public void setSubject(List<SubjectModel> subject) {
        this.subject = subject;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<BannerModel> getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(ArrayList<BannerModel> bannerImage) {
        this.bannerImage = bannerImage;
    }

    public ArrayList<ThumbnailModel> getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(ArrayList<ThumbnailModel> thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    @Override
    public String toString() {
        return "CoursesModel{" +
                "courseId=" + courseId +
                ", title='" + title + '\'' +
                ", institute=" + institute +
                ", subject=" + subject +
                ", intro='" + intro + '\'' +
                ", description='" + description + '\'' +
                ", bannerImage=" + bannerImage +
                ", thumbnailImage=" + thumbnailImage +
                '}';
    }
}
