package com.iwant.Models;

import com.iwant.Models.Catalogue.BannerModel;
import com.iwant.Models.Catalogue.ThumbnailModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "subjects")
public class SubjectModel {

    @MongoId
    private ObjectId subjectId;

    @Indexed
    private String title;

    private String intro;

    private String description;

    private ArrayList<BannerModel> bannerImage;

    private ArrayList<ThumbnailModel> thumbnailImage;

    private List<LessonModel> lesson;

    public SubjectModel() {
    }

    public ObjectId getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(ObjectId subjectId) {
        this.subjectId = subjectId;
    }

    public List<LessonModel> getLesson() {
        return lesson;
    }

    public void setLesson(List<LessonModel> lesson) {
        this.lesson = lesson;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<BannerModel> getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(ArrayList<BannerModel> bannerImage) {
        this.bannerImage = bannerImage;
    }

    public ArrayList<ThumbnailModel> getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(ArrayList<ThumbnailModel> thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    @Override
    public String toString() {
        return "SubjectModel{" +
                "subjectId=" + subjectId +
                ", lesson=" + lesson +
                ", title='" + title + '\'' +
                ", intro='" + intro + '\'' +
                ", description='" + description + '\'' +
                ", bannerImage=" + bannerImage +
                ", thumbnailImage=" + thumbnailImage +
                '}';
    }
}
