package com.iwant.Models.Catalogue;


import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document(collection = "Thumbnail")
public class ThumbnailModel {

    @MongoId
    private ObjectId thumbnailId;

    private String sequenceNo;

    private String tag;

    private String uri;

    public ThumbnailModel() {
    }

    public ObjectId getThumbnailId() {
        return thumbnailId;
    }

    public void setThumbnailId(ObjectId thumbnailId) {
        this.thumbnailId = thumbnailId;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "ThumbnailModel{" +
                "thumbnailId=" + thumbnailId +
                ", sequenceNo='" + sequenceNo + '\'' +
                ", tag='" + tag + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }
}
