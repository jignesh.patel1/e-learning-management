package com.iwant.Models.Catalogue;


import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document(collection = "Banner_detail")
public class BannerModel {

    @MongoId
    private ObjectId bannerId;

    private String sequenceNo;

    private String tag;

    private String uri;

    public BannerModel() {
    }

    public ObjectId getBannerId() {
        return bannerId;
    }

    public void setBannerId(ObjectId bannerId) {
        this.bannerId = bannerId;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "BannerModel{" +
                "bannerId=" + bannerId +
                ", sequenceNo='" + sequenceNo + '\'' +
                ", tag='" + tag + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }
}
