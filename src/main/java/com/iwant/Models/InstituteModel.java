package com.iwant.Models;


import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "Institute")
public class InstituteModel implements Serializable{

    @MongoId
    private ObjectId instituteId;

    @Indexed
    private String instituteName;

    private String originCountry;

    private String website;

    public InstituteModel() {
    }

    public ObjectId getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(ObjectId instituteId) {
        this.instituteId = instituteId;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return "InstituteModel{" +
                "instituteId=" + instituteId +
                ", instituteName='" + instituteName + '\'' +
                ", originCountry='" + originCountry + '\'' +
                ", website='" + website + '\'' +
                '}';
    }
}
