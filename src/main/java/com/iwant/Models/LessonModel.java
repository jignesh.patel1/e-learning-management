package com.iwant.Models;

import com.iwant.Models.Catalogue.BannerModel;
import com.iwant.Models.Catalogue.ThumbnailModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "lessons")
public class LessonModel {

    @MongoId
    private ObjectId lessonId;

    @Indexed
    private String title;

    private String intro;

    private String description;

    private ArrayList<BannerModel> bannerImage;

    private ArrayList<ThumbnailModel> thumbImage;

    private Date publishStart;

    private Date publishEnd;

    private List<TopicsModel> topics;

    private boolean isActive;

    public LessonModel() {
    }

    public ObjectId getLessonId() {
        return lessonId;
    }

    public void setLessonId(ObjectId lessonId) {
        this.lessonId = lessonId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<BannerModel> getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(ArrayList<BannerModel> bannerImage) {
        this.bannerImage = bannerImage;
    }

    public ArrayList<ThumbnailModel> getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(ArrayList<ThumbnailModel> thumbImage) {
        this.thumbImage = thumbImage;
    }

    public Date getPublishStart() {
        return publishStart;
    }

    public void setPublishStart(Date publishStart) {
        this.publishStart = publishStart;
    }

    public Date getPublishEnd() {
        return publishEnd;
    }

    public void setPublishEnd(Date publishEnd) {
        this.publishEnd = publishEnd;
    }

    public List<TopicsModel> getTopics() {
        return topics;
    }

    public void setTopics(List<TopicsModel> topics) {
        this.topics = topics;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "LessonModel{" +
                "lessonId=" + lessonId +
                ", title='" + title + '\'' +
                ", intro='" + intro + '\'' +
                ", description='" + description + '\'' +
                ", bannerImage=" + bannerImage +
                ", thumbImage=" + thumbImage +
                ", publishStart=" + publishStart +
                ", publishEnd=" + publishEnd +
                ", topics=" + topics +
                ", isActive='" + isActive + '\'' +
                '}';
    }
}
