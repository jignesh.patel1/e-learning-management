package com.iwant.Models.contentdetail;


import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;


import java.util.List;

@Document(collection = "contents")
public class ContentModel {

    @MongoId
    private ObjectId contentId;

    @Indexed
    private String title;

    private String description;

    private String provider;

    private String streamType;

    private String aspectRatio;

    private String audioLanguage;

    private Long duration;

    private String accessUrl;

    private String previewUrl;

    private String token;

    private List<DrmCertificateModel> drmCertificates;

    private List<CategoryModel> category;

    private boolean isActive;


    public ContentModel() {
    }

    public ObjectId getContentId() {
        return contentId;
    }

    public void setContentId(ObjectId contentId) {
        this.contentId = contentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStreamType() {
        return streamType;
    }

    public void setStreamType(String streamType) {
        this.streamType = streamType;
    }

    public String getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(String aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public String getAudioLanguage() {
        return audioLanguage;
    }

    public void setAudioLanguage(String audioLanguage) {
        this.audioLanguage = audioLanguage;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getAccessUrl() {
        return accessUrl;
    }

    public void setAccessUrl(String accessUrl) {
        this.accessUrl = accessUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<CategoryModel> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryModel> category) {
        this.category = category;
    }

    public List<DrmCertificateModel> getDrmCertificates() {
        return drmCertificates;
    }

    public void setDrmCertificates(List<DrmCertificateModel> drmCertificates) {
        this.drmCertificates = drmCertificates;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "ContentModel{" +
                "contentId=" + contentId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", provider='" + provider + '\'' +
                ", streamType='" + streamType + '\'' +
                ", aspectRatio='" + aspectRatio + '\'' +
                ", audioLanguage='" + audioLanguage + '\'' +
                ", duration=" + duration +
                ", accessUrl='" + accessUrl + '\'' +
                ", previewUrl='" + previewUrl + '\'' +
                ", token='" + token + '\'' +
                ", drmCertificates=" + drmCertificates +
                ", category=" + category +
                ", isActive=" + isActive +
                '}';
    }
}
