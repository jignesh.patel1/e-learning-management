package com.iwant.Models.contentdetail;


import org.bson.types.ObjectId;

public class DrmCertificateModel {

    private ObjectId drmId;

    private String provider;

    public DrmCertificateModel() {
    }

    public ObjectId getDrmId() {
        return drmId;
    }

    public void setDrmId(ObjectId drmId) {
        this.drmId = drmId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    @Override
    public String toString() {
        return "DrmCertificateModel{" +
                "drmId=" + drmId +
                ", provider='" + provider + '\'' +
                '}';
    }
}
