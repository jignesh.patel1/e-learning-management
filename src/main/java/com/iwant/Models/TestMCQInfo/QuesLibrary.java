package com.iwant.Models.TestMCQInfo;

import com.iwant.Models.Enumerations.AnsType;
import com.iwant.Models.LessonModel;
import com.iwant.Models.SubjectModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;

@Document(collection = "QuestionLibrary")
public class QuesLibrary {

    @MongoId
    private ObjectId quesId;
    private ArrayList<Question> question;
    private Enum<AnsType> answerType;
    private ArrayList<Answers> answers;
    private Number complexityLevel;
    private Integer marksCorrect;
    private Integer marksIncorrect;
    private ArrayList<LessonModel> lessonId;
    private ArrayList<SubjectModel> subjectId;

    public QuesLibrary() {
    }

    public ObjectId getQuesId() {
        return quesId;
    }

    public void setQuesId(ObjectId quesId) {
        this.quesId = quesId;
    }

    public ArrayList<Question> getQuestion() {
        return question;
    }

    public void setQuestion(ArrayList<Question> question) {
        this.question = question;
    }

    public Enum<AnsType> getAnswerType() {
        return answerType;
    }

    public void setAnswerType(Enum<AnsType> answerType) {
        this.answerType = answerType;
    }

    public ArrayList<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answers> answers) {
        this.answers = answers;
    }

    public Number getComplexityLevel() {
        return complexityLevel;
    }

    public void setComplexityLevel(Number complexityLevel) {
        this.complexityLevel = complexityLevel;
    }

    public Integer getMarksCorrect() {
        return marksCorrect;
    }

    public void setMarksCorrect(Integer marksCorrect) {
        this.marksCorrect = marksCorrect;
    }

    public Integer getMarksIncorrect() {
        return marksIncorrect;
    }

    public void setMarksIncorrect(Integer marksIncorrect) {
        this.marksIncorrect = marksIncorrect;
    }

    public ArrayList<LessonModel> getLessonId() {
        return lessonId;
    }

    public void setLessonId(ArrayList<LessonModel> lessonId) {
        this.lessonId = lessonId;
    }

    public ArrayList<SubjectModel> getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(ArrayList<SubjectModel> subjectId) {
        this.subjectId = subjectId;
    }

    @Override
    public String toString() {
        return "QuesLibrary{" +
                "quesId=" + quesId +
                ", question=" + question +
                ", answerType=" + answerType +
                ", answers=" + answers +
                ", complexityLevel=" + complexityLevel +
                ", marksCorrect=" + marksCorrect +
                ", marksIncorrect=" + marksIncorrect +
                ", lessonId=" + lessonId +
                ", subjectId=" + subjectId +
                '}';
    }
}

