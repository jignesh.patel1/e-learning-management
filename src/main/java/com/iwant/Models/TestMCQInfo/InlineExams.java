package com.iwant.Models.TestMCQInfo;

import com.iwant.Models.LessonModel;
import com.iwant.Models.SubjectModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.sql.Timestamp;
import java.util.ArrayList;

@Document(collection = "inlineExam")
public class InlineExams {

    @MongoId
    private ObjectId Id;

    private String title;
    private String sylaabusType;
    private ArrayList<SubjectModel> subject;
    private ArrayList<LessonModel> lesson;
    private String easyQuestion;
    private String moderateQuestion;
    private String hardQuestion;
    private Timestamp duration;
    private boolean isActive;

    public InlineExams() {
    }

    public ObjectId getId() {
        return Id;
    }

    public void setId(ObjectId id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSylaabusType() {
        return sylaabusType;
    }

    public void setSylaabusType(String sylaabusType) {
        this.sylaabusType = sylaabusType;
    }

    public ArrayList<SubjectModel> getSubject() {
        return subject;
    }

    public void setSubject(ArrayList<SubjectModel> subject) {
        this.subject = subject;
    }

    public ArrayList<LessonModel> getLesson() {
        return lesson;
    }

    public void setLesson(ArrayList<LessonModel> lesson) {
        this.lesson = lesson;
    }

    public String getEasyQuestion() {
        return easyQuestion;
    }

    public void setEasyQuestion(String easyQuestion) {
        this.easyQuestion = easyQuestion;
    }

    public String getModerateQuestion() {
        return moderateQuestion;
    }

    public void setModerateQuestion(String moderateQuestion) {
        this.moderateQuestion = moderateQuestion;
    }

    public String getHardQuestion() {
        return hardQuestion;
    }

    public void setHardQuestion(String hardQuestion) {
        this.hardQuestion = hardQuestion;
    }

    public Timestamp getDuration() {
        return duration;
    }

    public void setDuration(Timestamp duration) {
        this.duration = duration;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "InlineExams{" +
                "Id=" + Id +
                ", title='" + title + '\'' +
                ", sylaabusType='" + sylaabusType + '\'' +
                ", subject=" + subject +
                ", lesson=" + lesson +
                ", easyQuestion='" + easyQuestion + '\'' +
                ", moderateQuestion='" + moderateQuestion + '\'' +
                ", hardQuestion='" + hardQuestion + '\'' +
                ", duration=" + duration +
                ", isActive=" + isActive +
                '}';
    }
}
