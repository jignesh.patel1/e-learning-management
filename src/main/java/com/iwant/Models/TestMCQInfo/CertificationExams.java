package com.iwant.Models.TestMCQInfo;

import com.iwant.Models.Catalogue.ThumbnailModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;

@Document(collection = "certificateExam")
public class CertificationExams {

    @MongoId
    private ObjectId Id;

    private String title;
    private String intro;
    private ArrayList<ThumbnailModel> thumbImage;
}
