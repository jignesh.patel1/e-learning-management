package com.iwant.Models.TestMCQInfo;


public class Answers {

    private String caption;
    private String value;
    private boolean isCorrect;
    private String solution;

    public Answers() {
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    @Override
    public String toString() {
        return "Answers{" +
                "caption='" + caption + '\'' +
                ", value='" + value + '\'' +
                ", isCorrect=" + isCorrect +
                ", solution='" + solution + '\'' +
                '}';
    }
}
