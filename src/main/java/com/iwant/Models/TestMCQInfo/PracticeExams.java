package com.iwant.Models.TestMCQInfo;

import com.iwant.Models.Catalogue.ThumbnailModel;
import com.iwant.Models.LessonModel;
import com.iwant.Models.SubjectModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;

@Document(collection = "PracticeExams")
public class PracticeExams {

    @MongoId
    private ObjectId pracExamId;
    private String title;
    private String intro;
    private ArrayList<ThumbnailModel> thumbImage;
    private ArrayList<SubjectModel> subject;
    private ArrayList<LessonModel> lesson;
    private ArrayList<Question> ques;

    public PracticeExams() {
    }


}