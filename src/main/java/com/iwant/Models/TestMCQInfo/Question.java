package com.iwant.Models.TestMCQInfo;


import java.sql.Timestamp;

public class Question {

    private Integer number;
    private String value;
    private Timestamp duration;
    private String solution;

    public Question() {
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Timestamp getDuration() {
        return duration;
    }

    public void setDuration(Timestamp duration) {
        this.duration = duration;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    @Override
    public String toString() {
        return "Question{" +
                "number=" + number +
                ", value='" + value + '\'' +
                ", duration=" + duration +
                ", solution='" + solution + '\'' +
                '}';
    }
}
