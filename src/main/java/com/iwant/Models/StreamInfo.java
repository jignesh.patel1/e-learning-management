package com.iwant.Models;


public class StreamInfo {

    private String seqNumber;

    private String title;

    private  String vodContent;

    private String skippable;

    private String skipDuration;

    public String getSeqNumber() {
        return seqNumber;
    }

    public void setSeqNumber(String seqNumber) {
        this.seqNumber = seqNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVodContent() {
        return vodContent;
    }

    public void setVodContent(String vodContent) {
        this.vodContent = vodContent;
    }

    public String getSkippable() {
        return skippable;
    }

    public void setSkippable(String skippable) {
        this.skippable = skippable;
    }

    public String getSkipDuration() {
        return skipDuration;
    }

    public void setSkipDuration(String skipDuration) {
        this.skipDuration = skipDuration;
    }
}
