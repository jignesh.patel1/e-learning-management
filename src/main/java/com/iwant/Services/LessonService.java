package com.iwant.Services;


import com.iwant.Models.LessonModel;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface LessonService {

    LessonModel AddLessons(LessonModel lesson);
    List<LessonModel> getAllLesson();
    Optional<LessonModel> getLessonById(ObjectId lessonId);
    LessonModel updateLesson(LessonModel lesson);
    void deleteAllLesson();
    void deleteLessonById(ObjectId lessonId);

}
