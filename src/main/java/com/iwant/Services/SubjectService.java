package com.iwant.Services;


import com.iwant.Models.SubjectModel;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface SubjectService {

    SubjectModel AddSubjects(SubjectModel subjects);
    List<SubjectModel> getAllSubjects();
    Optional<SubjectModel> getSubjectById(ObjectId subjectId);
    SubjectModel updateSubject(SubjectModel subjects);
    void deleteAllSubjects();
    void deleteSubjectById(ObjectId subjectId);
}
