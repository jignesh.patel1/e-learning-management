package com.iwant.Services.Impl;

import com.iwant.Models.TopicsModel;
import com.iwant.Repo.TopicRepository;
import com.iwant.Services.TopicService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TopisServiceImpl implements TopicService{

    @Autowired
    TopicRepository top;

    @Override
    public TopicsModel AddTopic(TopicsModel topics){
        return top.save(topics);
    }

    @Override
    public List<TopicsModel> getAllTopic(){
        return top.findAll();
    }

    @Override
    public Optional<TopicsModel> getTopicById(ObjectId topicId){
        return top.findById(topicId);
    }

    @Override
    public TopicsModel updateTopic(TopicsModel topics){
        return top.save(topics);
    }

    @Override
    public void deleteAllTopic(){
        top.deleteAll();
    }

    @Override
    public void deleteTopicById(ObjectId topicId){
        top.deleteById(topicId);
    }

}
