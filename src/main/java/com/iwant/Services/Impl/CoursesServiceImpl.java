package com.iwant.Services.Impl;

import com.iwant.Models.CoursesModel;
import com.iwant.Models.InstituteModel;
import com.iwant.Repo.CoursesRepository;
import com.iwant.Services.CoursesService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CoursesServiceImpl implements CoursesService{

    @Autowired
    CoursesRepository cors;

    @Override
    public CoursesModel AddCourses(CoursesModel courses){
        return cors.save(courses);
    }

    @Override
    public List<CoursesModel> getAllCourses(){
        return cors.findAll();
    }

    @Override
    public Optional<CoursesModel> getCourseById(ObjectId courseId){
        return cors.findById(courseId);
    }

    @Override
    public CoursesModel updateCourse(CoursesModel courses){
        return cors.save(courses);
    }

    @Override
    public void deleteAllCourses(){
        cors.deleteAll();
    }

    @Override
    public void deleteCourseById(ObjectId courseId){
        cors.deleteById(courseId);
    }

    @Override
    public List<CoursesModel> getCoursesByInstituteName(String instName){
        List<CoursesModel> allCourse = cors.findAll();
        List<CoursesModel> coursesSelectedList = null;

        for(int i=0; i< allCourse.size(); i++){
            if(null != allCourse.get(i).getInstitute()
                    && null != allCourse.get(i).getInstitute().getInstituteName()){
                if(allCourse.get(i).getInstitute().getInstituteName().equals(instName)){
                    coursesSelectedList = new ArrayList<>();
                    coursesSelectedList.add(0, allCourse.get(i));
                }
                }
        }

        return coursesSelectedList;
    }


}
