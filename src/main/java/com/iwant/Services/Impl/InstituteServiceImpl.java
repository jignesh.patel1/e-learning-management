package com.iwant.Services.Impl;

import com.iwant.Models.InstituteModel;
import com.iwant.Repo.InstituteRepository;
import com.iwant.Services.InstituteService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class InstituteServiceImpl implements InstituteService{

    @Autowired
    InstituteRepository inst;

    @Override
    public InstituteModel AddInstitute(InstituteModel institutes){
        return inst.save(institutes);
    }

    @Override
    public List<InstituteModel> getAllInstitutes(){
        return inst.findAll();
    }

    @Override
    public Optional<InstituteModel> getInstituteById(ObjectId instituteId){
        return inst.findById(instituteId);
    }

    @Override
    public InstituteModel updateInstitute(InstituteModel institutes){
        return inst.save(institutes);
    }

    @Override
    public void deleteAllInstitutes(){
        inst.deleteAll();

    }

    @Override
    public void deleteInstituteById(ObjectId instituteId){
        inst.deleteById(instituteId);
    }
}
