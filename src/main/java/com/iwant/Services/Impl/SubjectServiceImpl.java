package com.iwant.Services.Impl;


import com.iwant.Models.SubjectModel;
import com.iwant.Repo.SubjectRepository;
import com.iwant.Services.SubjectService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService{

    @Autowired
    SubjectRepository subj;

    @Override
    public SubjectModel AddSubjects(SubjectModel subjects){
        return subj.save(subjects);
    }

    @Override
    public List<SubjectModel> getAllSubjects(){
        return subj.findAll();
    }

    @Override
    public Optional<SubjectModel> getSubjectById(ObjectId subjectId){
        return subj.findById(subjectId);
    }

    @Override
    public SubjectModel updateSubject(SubjectModel subjects){
        return subj.save(subjects);
    }

    @Override
    public void deleteAllSubjects(){
        subj.deleteAll();
    }

    @Override
    public void deleteSubjectById(ObjectId subjectId){
        subj.deleteById(subjectId);
    }
}
