package com.iwant.Services.Impl;


import com.iwant.Models.contentdetail.ContentModel;
import com.iwant.Repo.ContentRepository;
import com.iwant.Services.ContentService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ContentServiceImpl implements ContentService{

    @Autowired
    ContentRepository con;

    @Override
    public ContentModel AddContent(ContentModel content){
        return con.save(content);
    }

    @Override
    public List<ContentModel> getAllContent(){
        return con.findAll();
    }

    @Override
    public Optional<ContentModel> getContentById(ObjectId contentId){
        return con.findById(contentId);
    }

    @Override
    public ContentModel updateContent(ContentModel content){
        return con.save(content);
    }

    @Override
    public void deleteAllContent(){
        con.deleteAll();
    }

    @Override
    public void deleteContentById(ObjectId contentId){
        con.deleteById(contentId);
    }

}
