package com.iwant.Services.Impl;

import com.iwant.Models.LessonModel;
import com.iwant.Repo.LessonRepository;
import com.iwant.Services.LessonService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class LessonServiceImpl implements LessonService{

    @Autowired
    LessonRepository les;

    @Override
    public LessonModel AddLessons(LessonModel lesson){
        return les.save(lesson);
    }

    @Override
    public List<LessonModel> getAllLesson(){
        return les.findAll();
    }

    @Override
    public Optional<LessonModel> getLessonById(ObjectId lessonId){
        return les.findById(lessonId);
    }

    @Override
    public LessonModel updateLesson(LessonModel lesson){
        return les.save(lesson);
    }
    @Override
    public void deleteAllLesson(){
        les.deleteAll();
    }

    @Override
    public void deleteLessonById(ObjectId lessonId){
        les.deleteById(lessonId);
    }

}
