package com.iwant.Services.Impl;


import com.iwant.Models.SvodProductModel;
import com.iwant.Repo.SvodProductRepository;
import com.iwant.Services.SvodProductService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SvodProductServiceImpl implements SvodProductService{

    @Autowired
    SvodProductRepository con;

    @Override
    public SvodProductModel addSvodProduct(SvodProductModel svodProduct){
        return con.save(svodProduct);
    }

    @Override
    public List<SvodProductModel> getAllSvodProduct(){
        return con.findAll();
    }

    @Override
    public Optional<SvodProductModel> getSvodProductById(ObjectId SvodProductId){
        return con.findById(SvodProductId);
    }

    @Override
    public SvodProductModel updateSvodProduct(SvodProductModel svodProduct){
        return con.save(svodProduct);
    }

    @Override
    public void deleteAllSvodProduct(){
        con.deleteAll();
    }

    @Override
    public void deleteSvodProductById(ObjectId svodProductId){
        con.deleteById(svodProductId);
    }

}
