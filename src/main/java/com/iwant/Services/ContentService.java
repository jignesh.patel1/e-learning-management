package com.iwant.Services;


import com.iwant.Models.contentdetail.ContentModel;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface ContentService {

    ContentModel AddContent(ContentModel content);
    List<ContentModel> getAllContent();
    Optional<ContentModel> getContentById(ObjectId contentId);
    ContentModel updateContent(ContentModel content);
    void deleteAllContent();
    void deleteContentById(ObjectId contentId);
}
