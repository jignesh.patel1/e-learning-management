package com.iwant.Services;


import com.iwant.Models.CoursesModel;
import com.iwant.Models.InstituteModel;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface CoursesService {

    CoursesModel AddCourses(CoursesModel courses);
    List<CoursesModel> getAllCourses();
    Optional<CoursesModel> getCourseById(ObjectId courseId);
    CoursesModel updateCourse(CoursesModel courses);
    void deleteAllCourses();
    void deleteCourseById(ObjectId courseId);
    List<CoursesModel> getCoursesByInstituteName(String instName);
}
