package com.iwant.Services.TestMCQServices;


import com.iwant.Models.TestMCQInfo.CertificationExams;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface CertificationService {

    CertificationExams AddCertifyExam(CertificationExams certifyExams);
    List<CertificationExams> getAllCertifyExam();
    CertificationExams updateCertifyExam(CertificationExams certifyExams);
    Optional<CertificationExams> getCertifyExamById(ObjectId Id);
    void deleteAllCertificationExams();
    void deleteCertificationExamsById(ObjectId Id);

}
