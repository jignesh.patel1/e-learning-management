package com.iwant.Services.TestMCQServices;


import com.iwant.Models.TestMCQInfo.QuesLibrary;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface QuesLibraryService {


    QuesLibrary AddQuestion(QuesLibrary question);
    List<QuesLibrary> getAllQuestion();
    QuesLibrary updateQuestion(QuesLibrary question);
    Optional<QuesLibrary> getQuestionById(ObjectId quesId);
    void deleteAllQuestion();
    void deleteQuestionById(ObjectId quesId);

}