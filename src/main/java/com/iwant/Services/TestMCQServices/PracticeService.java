package com.iwant.Services.TestMCQServices;


import com.iwant.Models.TestMCQInfo.PracticeExams;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface PracticeService {

    PracticeExams AddPracticePaper(PracticeExams paper);
    List<PracticeExams> getALLPracticePaper();
    PracticeExams updatePracticePaper(PracticeExams paper);
    Optional<PracticeExams> getPracticeExamById(ObjectId pracExamId);
    void deleteAllPracticeExam();
    void deletePracticeById(ObjectId pracExamId);

}
