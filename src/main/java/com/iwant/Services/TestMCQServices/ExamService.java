package com.iwant.Services.TestMCQServices;


import com.iwant.Models.TestMCQInfo.ExamAttempts;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface ExamService {

    ExamAttempts AddExamAttempts(ExamAttempts attempts);
    List<ExamAttempts> getAllExamAttempts();
    ExamAttempts updateExamAttempts(ExamAttempts attempts);
    Optional<ExamAttempts> getExamAttemptsById(ObjectId Id);
    void deleteAllAttempts();
    void deleteAttemptsById(ObjectId Id);

}
