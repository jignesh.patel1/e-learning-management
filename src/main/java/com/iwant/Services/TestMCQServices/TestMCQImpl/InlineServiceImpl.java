package com.iwant.Services.TestMCQServices.TestMCQImpl;


import com.iwant.Models.TestMCQInfo.InlineExams;
import com.iwant.Repo.TestMCQRepo.InlineRepository;
import com.iwant.Services.TestMCQServices.InlineService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class InlineServiceImpl implements InlineService {

    @Autowired
    InlineRepository inline;

    @Override
    public InlineExams AddInlinePaper(InlineExams paper){
        return inline.save(paper);
    }

    @Override
    public InlineExams updateInlinePaper(InlineExams paper){
        return inline.save(paper);
    }

    @Override
    public List<InlineExams> getAllInlinePaper(){
        return inline.findAll();
    }

    @Override
    public Optional<InlineExams> getInlineExamById(ObjectId Id){
        return inline.findById(Id);
    }

    @Override
    public void deleteAllInline(){
        inline.deleteAll();
    }

    @Override
    public void deleteInlineById(ObjectId Id){
        inline.deleteById(Id);
    }

}
