package com.iwant.Services.TestMCQServices.TestMCQImpl;

import com.iwant.Models.TestMCQInfo.QuesLibrary;
import com.iwant.Repo.TestMCQRepo.QuesRepository;
import com.iwant.Services.TestMCQServices.QuesLibraryService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;


public class QuesLibServiceImpl implements QuesLibraryService {

    @Autowired
    QuesRepository ques;


    @Override
    public QuesLibrary AddQuestion(QuesLibrary question){
        return ques.save(question);
    }

    @Override
    public QuesLibrary updateQuestion(QuesLibrary question){
        return ques.save(question);
    }

    @Override
    public List<QuesLibrary> getAllQuestion(){
        return ques.findAll();
    }

    @Override
    public Optional<QuesLibrary> getQuestionById(ObjectId quesId){
        return ques.findById(quesId);
    }

    @Override
    public void deleteAllQuestion(){
        ques.deleteAll();
    }

    @Override
    public void deleteQuestionById(ObjectId quesId){
        ques.deleteById(quesId);
    }


}
