package com.iwant.Services.TestMCQServices.TestMCQImpl;


import com.iwant.Models.TestMCQInfo.CertificationExams;
import com.iwant.Repo.TestMCQRepo.CertificationRepository;
import com.iwant.Services.TestMCQServices.CertificationService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class CertificationServiceImpl implements CertificationService {

    @Autowired
    CertificationRepository certify;

    @Override
    public CertificationExams AddCertifyExam(CertificationExams certifyExams){
        return certify.save(certifyExams);
    }

    @Override
    public CertificationExams updateCertifyExam(CertificationExams certifyExams){
        return certify.save(certifyExams);
    }

    @Override
    public List<CertificationExams> getAllCertifyExam(){
        return certify.findAll();
    }

    @Override
    public Optional<CertificationExams> getCertifyExamById(ObjectId Id){
        return certify.findById(Id);
    }

    @Override
    public void deleteAllCertificationExams(){
        certify.deleteAll();
    }

    @Override
    public void deleteCertificationExamsById(ObjectId Id){
        certify.deleteById(Id);
    }

}
