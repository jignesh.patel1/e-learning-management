package com.iwant.Services.TestMCQServices.TestMCQImpl;


import com.iwant.Models.TestMCQInfo.ExamAttempts;
import com.iwant.Repo.TestMCQRepo.ExamRepository;
import com.iwant.Services.TestMCQServices.ExamService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class ExamServiceImpl implements ExamService {

    @Autowired
    ExamRepository exams;

    @Override
    public ExamAttempts AddExamAttempts(ExamAttempts attempts){
        return exams.save(attempts);
    }

    @Override
    public List<ExamAttempts> getAllExamAttempts(){
        return exams.findAll();
    }

    @Override
    public Optional<ExamAttempts> getExamAttemptsById(ObjectId Id){
        return exams.findById(Id);
    }

    @Override
    public ExamAttempts updateExamAttempts(ExamAttempts attempts){
        return exams.save(attempts);
    }

    @Override
    public void deleteAllAttempts(){
        exams.deleteAll();
    }

    @Override
    public void deleteAttemptsById(ObjectId Id){
        exams.deleteById(Id);
    }

}
