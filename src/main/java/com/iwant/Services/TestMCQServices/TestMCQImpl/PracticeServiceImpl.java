package com.iwant.Services.TestMCQServices.TestMCQImpl;


import com.iwant.Models.TestMCQInfo.PracticeExams;
import com.iwant.Repo.TestMCQRepo.PracticeRepository;
import com.iwant.Services.TestMCQServices.PracticeService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class PracticeServiceImpl implements PracticeService {

    @Autowired
    PracticeRepository prac;

    @Override
    public PracticeExams AddPracticePaper(PracticeExams paper){
        return prac.save(paper);
    }

    @Override
    public List<PracticeExams> getALLPracticePaper(){
        return prac.findAll();
    }

    @Override
    public Optional<PracticeExams> getPracticeExamById(ObjectId pracExamId){
        return prac.findById(pracExamId);
    }

    @Override
    public PracticeExams updatePracticePaper(PracticeExams paper){
        return prac.save(paper);
    }

    @Override
    public void deleteAllPracticeExam(){
        prac.deleteAll();
    }

    @Override
    public void deletePracticeById(ObjectId pracExamId){
        prac.deleteById(pracExamId);
    }

}
