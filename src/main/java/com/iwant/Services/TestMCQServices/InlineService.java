package com.iwant.Services.TestMCQServices;


import com.iwant.Models.TestMCQInfo.InlineExams;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface InlineService {

    InlineExams AddInlinePaper(InlineExams paper);
    List<InlineExams> getAllInlinePaper();
    InlineExams updateInlinePaper(InlineExams paper);
    Optional<InlineExams> getInlineExamById(ObjectId Id);
    void deleteAllInline();
    void deleteInlineById(ObjectId Id);

}
