package com.iwant.Services;



import com.iwant.Models.SvodProductModel;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface SvodProductService {

    SvodProductModel addSvodProduct(SvodProductModel svodProduct);
    List<SvodProductModel> getAllSvodProduct();
    Optional<SvodProductModel> getSvodProductById(ObjectId svodProductId);
    SvodProductModel updateSvodProduct(SvodProductModel svodProduct);
    void deleteAllSvodProduct();
    void deleteSvodProductById(ObjectId svodProductId);
}
