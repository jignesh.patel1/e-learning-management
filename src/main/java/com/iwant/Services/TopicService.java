package com.iwant.Services;


import com.iwant.Models.TopicsModel;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface TopicService {

    TopicsModel AddTopic(TopicsModel topics);
    List<TopicsModel> getAllTopic();
    Optional<TopicsModel> getTopicById(ObjectId topicId);
    TopicsModel updateTopic(TopicsModel topics);
    void deleteAllTopic();
    void deleteTopicById(ObjectId topicId);
}
