package com.iwant.Services;


import com.iwant.Models.InstituteModel;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;


public interface InstituteService {

    InstituteModel AddInstitute(InstituteModel institutes);
    List<InstituteModel> getAllInstitutes();
    Optional<InstituteModel> getInstituteById(ObjectId instituteId);
    InstituteModel updateInstitute(InstituteModel institutes);
    void deleteAllInstitutes();
    void deleteInstituteById(ObjectId instituteId);

}
