package com.iwant.Controllers;

import com.iwant.Models.CoursesModel;
import com.iwant.Models.InstituteModel;
import com.iwant.Services.CoursesService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class CoursesController {

    @Autowired
    CoursesService service;

    @RequestMapping(value = "/courses/add", method = RequestMethod.POST)
    public CoursesModel AddCourses(@RequestBody CoursesModel course){
        return service.AddCourses(course);
    }

    @GetMapping(value = "/courses/getAll")
    public List<CoursesModel> getAllCourses(){
        return service.getAllCourses();
    }

    @GetMapping(value = "/courses/getcourse/{id}")
    public CoursesModel getById(@PathVariable("id") ObjectId courseId) throws Exception{
        Optional<CoursesModel> c = service.getCourseById(courseId);
        if(!c.isPresent())
            throw new Exception("cannot find the course with id {}" + courseId);
        return c.get();
    }

    @PutMapping(value = "/courses/update/{id}")
    public CoursesModel updateCourses(@PathVariable("id") ObjectId courseId, @Valid@RequestBody CoursesModel course) {
        course.setCourseId(courseId);
        return service.updateCourse(course);
    }

    @DeleteMapping(value = "/courses/deleteAll")
    public void deleteAllCourses(){
        service.deleteAllCourses();
    }

    @DeleteMapping(value = "/courses/deletecourse/{id}")
    public void deleteById(@PathVariable("id") ObjectId courseId) throws Exception{
        Optional<CoursesModel> c = service.getCourseById(courseId);
        if(!c.isPresent())
            throw new Exception("cannot find course with id {}" +courseId);
        service.deleteCourseById(courseId);
    }

    @GetMapping(value = "/courses/get/instituteName/{name}")
    public List<CoursesModel> getCoursesByInstituteName(@PathVariable("name") String instName){
        return service.getCoursesByInstituteName(instName);
    }

}
