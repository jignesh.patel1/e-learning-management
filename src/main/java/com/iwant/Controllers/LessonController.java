package com.iwant.Controllers;

import com.iwant.Models.LessonModel;
import com.iwant.Services.LessonService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class LessonController {

    @Autowired
    LessonService service;

    @RequestMapping(value = "/lesson/add", method = RequestMethod.POST)
    public LessonModel AddLesson(@RequestBody LessonModel lesson){
        return service.AddLessons(lesson);
    }

    @GetMapping(value = "/lesson/getAll")
    public List<LessonModel> getAllLesson(){
        return service.getAllLesson();
    }

    @GetMapping(value = "/lesson/get/{id}")
    public LessonModel getLessonById(@PathVariable("id") ObjectId lessonId) throws Exception{
        Optional<LessonModel> l = service.getLessonById(lessonId);
        if(!l.isPresent())
            throw new Exception("cannot find lesson with id {}" +lessonId);
        return l.get();
    }

    @PutMapping(value = "/lesson/update/{id}")
    public LessonModel updateLesson(@PathVariable("id") ObjectId lessonId, @Valid@RequestBody LessonModel lesson){
        lesson.setLessonId(lessonId);
        return service.updateLesson(lesson);
    }

    @DeleteMapping(value = "/lesson/deletAll")
    public void deleteAllLesson(){
        service.deleteAllLesson();
    }

    @DeleteMapping(value = "/lesson/delete/{id}")
    public void deleteById(@PathVariable("id") ObjectId lessonId) throws Exception{
        Optional<LessonModel> l = service.getLessonById(lessonId);
        if(!l.isPresent())
            throw new Exception("cannot find lesson with id {}" +lessonId);
        service.deleteLessonById(lessonId);
    }

}
