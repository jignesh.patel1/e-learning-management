package com.iwant.Controllers;

import com.iwant.Models.SubjectModel;
import com.iwant.Services.SubjectService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class SubjectController {
    @Autowired
    SubjectService service;

    @RequestMapping(value = "/subject/add", method = RequestMethod.POST)
    public SubjectModel AddSubjects(@RequestBody SubjectModel subject){
        return service.AddSubjects(subject);
    }

    @GetMapping(value = "/subjects/getAll")
    public List<SubjectModel> getAllSubjects(){
        return service.getAllSubjects();
    }

    @GetMapping(value = "/subjects/getsubject/{id}")
    public SubjectModel getSubjectById(@PathVariable("id")ObjectId subjectId) throws Exception{
        Optional<SubjectModel> s = service.getSubjectById(subjectId);
        if(!s.isPresent())
            throw new Exception("cannot find the subject with id {}" +subjectId);
        return s.get();
    }

    @PutMapping(value = "/subject/update/{id}")
    public SubjectModel updateSubject(@PathVariable("id") ObjectId subjectId, @Valid@RequestBody SubjectModel subject){
        subject.setSubjectId(subjectId);
        return service.updateSubject(subject);
    }

    @DeleteMapping(value = "/subject/deleteAll")
    public void deleteAllSubjects(){
        service.deleteAllSubjects();
    }

    @DeleteMapping(value = "/subject/deletesubject/{id}")
    public void deleteById(@PathVariable("id") ObjectId subjectId) throws Exception{
        Optional<SubjectModel> s = service.getSubjectById(subjectId);
        if(!s.isPresent())
            throw new Exception("cannot find subject with id {}" +subjectId);
        service.deleteSubjectById(subjectId);
    }

}
