package com.iwant.Controllers;


import com.iwant.Models.SvodProductModel;
import com.iwant.Services.SvodProductService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class SvodProductController {

    @Autowired
    SvodProductService service;

    @RequestMapping(value = "/svodProduct/add", method = RequestMethod.POST)
    public SvodProductModel addSvodProduct(@RequestBody SvodProductModel svodProduct){
        return service.addSvodProduct(svodProduct);
    }

    @GetMapping(value = "/svodProduct/getAll")
    public List<SvodProductModel> getAllSvodProduct(){
        return service.getAllSvodProduct();
    }

    @GetMapping(value = "/svodProduct/get/{id}")
    public SvodProductModel getSvodProductById(@PathVariable("id")ObjectId productId) throws Exception{
        Optional<SvodProductModel> c = service.getSvodProductById(productId);
        if(!c.isPresent())
            throw new Exception("cannot find svodProduct with id {}" +productId);
        return c.get();
    }

    @PutMapping(value = "/svodProduct/update/{id}")
    public SvodProductModel updateSvodProduct(@PathVariable("id") ObjectId svodProductId, @Valid@RequestBody SvodProductModel product){
        product.setSvodProductId(svodProductId);
        return service.updateSvodProduct(product);
    }

    @DeleteMapping(value = "/svodProduct/deleteAll")
    public void deleteAll(){
        service.deleteAllSvodProduct();
    }

    @DeleteMapping(value = "/svodProduct/delete/{id}")
    public void deleteSvodProductById(@PathVariable("id") ObjectId svodProductId) throws Exception{
        Optional<SvodProductModel> c = service.getSvodProductById(svodProductId);
        if(!c.isPresent())
            throw new Exception("cannot find svodProduct with id {}" +svodProductId);
        service.deleteSvodProductById(svodProductId);
    }

}
