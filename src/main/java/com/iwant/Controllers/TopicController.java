package com.iwant.Controllers;

import com.iwant.Models.TopicsModel;
import com.iwant.Services.TopicService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class TopicController {

    @Autowired
    TopicService service;

    @RequestMapping(value = "/topic/add", method = RequestMethod.POST)
    public TopicsModel AddTopic(@RequestBody TopicsModel topics){
        return service.AddTopic(topics);
    }

    @GetMapping(value = "/topic/getAll")
    public List<TopicsModel> getAllTopic(){
        return service.getAllTopic();
    }

    @GetMapping(value = "/topic/get/{id}")
    public TopicsModel getTopicById(@PathVariable("id")ObjectId topicId) throws Exception{
        Optional<TopicsModel> t = service.getTopicById(topicId);
        if(!t.isPresent())
            throw new Exception("cannot find topic with id {}" +topicId);
        return t.get();
    }

    @PutMapping(value = "/topic/update/id")
    public TopicsModel updateTopic(@PathVariable("id") ObjectId topicId, @Valid@RequestBody TopicsModel topics){
        topics.setTopicId(topicId);
        return service.updateTopic(topics);
    }

    @DeleteMapping(value = "/topic/deleteAll")
    public void deleteAllTopic(){
        service.deleteAllTopic();
    }

    @DeleteMapping(value = "/topic/delete/{id}")
    public void deleteById(@PathVariable ObjectId topicId) throws Exception{
        Optional<TopicsModel> t = service.getTopicById(topicId);
        if(!t.isPresent())
            throw new Exception("cannot find topic wit id {}" +topicId);
        service.deleteTopicById(topicId);
    }

}
