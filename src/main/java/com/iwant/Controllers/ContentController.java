package com.iwant.Controllers;

import com.iwant.Models.contentdetail.ContentModel;
import com.iwant.Services.ContentService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class ContentController {

    @Autowired
    ContentService service;

    @RequestMapping(value = "/content/Add", method = RequestMethod.POST)
    public ContentModel AddContent(@RequestBody ContentModel content){
        return service.AddContent(content);
    }

    @GetMapping(value = "/content/getAll")
    public List<ContentModel> getAllContent(){
        return service.getAllContent();
    }

    @GetMapping(value = "/content/get/{id}")
    public ContentModel getContentById(@PathVariable("id")ObjectId contentId) throws Exception{
        Optional<ContentModel> c = service.getContentById(contentId);
        if(!c.isPresent())
            throw new Exception("cannot find content with id {}" +contentId);
        return c.get();
    }

    @PutMapping(value = "/content/update/{id}")
    public ContentModel updateContent(@PathVariable("id") ObjectId contentId, @Valid@RequestBody ContentModel content){
        content.setContentId(contentId);
        return service.updateContent(content);
    }

    @DeleteMapping(value = "/content/deleteAll")
    public void deleteAll(){
        service.deleteAllContent();
    }

    @DeleteMapping(value = "/content/delete/{id}")
    public void deleteContentById(@PathVariable("id") ObjectId contentId) throws Exception{
        Optional<ContentModel> c = service.getContentById(contentId);
        if(!c.isPresent())
            throw new Exception("cannot find content with id {}" +contentId);
        service.deleteContentById(contentId);
    }

}
