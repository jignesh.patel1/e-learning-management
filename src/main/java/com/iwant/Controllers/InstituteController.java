package com.iwant.Controllers;


import com.iwant.Models.InstituteModel;
import com.iwant.Services.InstituteService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class InstituteController {

    @Autowired
    InstituteService service;

    @RequestMapping(value = "/institute/add", method = RequestMethod.POST)
    public InstituteModel createInstitute(@RequestBody InstituteModel institute){
        return service.AddInstitute(institute);
    }

    @GetMapping(value = "/institute/getAll")
    public List<InstituteModel> getAllInstitute(){
        return service.getAllInstitutes();
    }

    @GetMapping(value = "/institute/get/{id}")
    public InstituteModel getByInstituteId(@PathVariable("id")ObjectId instituteId) throws Exception{
        Optional<InstituteModel> i = service.getInstituteById(instituteId);
        if(!i.isPresent())
            throw new Exception("cannot find institute with id {}" + instituteId);
        return i.get();

    }

    @PutMapping(value = "/institute/update/{id}")
    public InstituteModel updateInstitute(@PathVariable("id") ObjectId instituteId, @Valid@RequestBody InstituteModel institute){
        institute.setInstituteId(instituteId);
        return service.updateInstitute(institute);
    }

    @DeleteMapping(value = "/institute/deleteAll")
    public void deleteAllInstitute(){
        service.deleteAllInstitutes();
    }

    @DeleteMapping(value = "/institute/delete/{id}")
    public void deleteByInstituteId(@PathVariable("id") ObjectId instituteId) throws Exception{
        Optional<InstituteModel> i = service.getInstituteById(instituteId);
        if(!i.isPresent())
            throw new Exception("cannot find institute with id {}" + instituteId);
        service.deleteInstituteById(instituteId);
    }
}
