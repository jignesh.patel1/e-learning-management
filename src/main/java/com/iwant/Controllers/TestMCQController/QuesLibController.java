package com.iwant.Controllers.TestMCQController;

import com.iwant.Models.TestMCQInfo.QuesLibrary;
import com.iwant.Services.TestMCQServices.QuesLibraryService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class QuesLibController {

    @Autowired
    QuesLibraryService service;

    @PostMapping(value = "/QuestionLibrary/add")
    public QuesLibrary createQuesLib(@RequestBody QuesLibrary question){
        return service.AddQuestion(question);
    }

    @GetMapping(value = "/QuestionLib/getAll")
    public List<QuesLibrary> getAll(){
        return service.getAllQuestion();
    }

    @PutMapping(value = "/QuestionLib/update/{id}")
    public QuesLibrary updateQuestion(@PathVariable("id") ObjectId quesId, @Valid@RequestBody QuesLibrary question){
        question.setQuesId(quesId);
        return service.updateQuestion(question);
    }

    @GetMapping(value = "/QuestionLib/getQues/{id}")
    public QuesLibrary getById(@PathVariable("id") ObjectId quesId) throws Exception{
        Optional<QuesLibrary> que = service.getQuestionById(quesId);
        if(!que.isPresent())
            throw new Exception("cannot find the course with id {}" + quesId);
        return que.get();

    }

    @DeleteMapping(value = "/QuestionLib/deleteAll")
    public void deleteAll(){
        service.deleteAllQuestion();
    }

    @DeleteMapping(value = "/QuestionLib/deleteQues/{id}")
    public void deleteById(@PathVariable("id") ObjectId quesId) throws Exception{
        Optional<QuesLibrary> que = service.getQuestionById(quesId);
        if(!que.isPresent())
            throw new Exception("cannot find the course with id {}" + quesId);
        service.deleteQuestionById(quesId);
    }

}
